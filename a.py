class A():
    order = 10
    
    @staticmethod
    def foo(x):
        return x**2
    class_func = foo
    
    def __init__(self, func):
        A.class_func = func
    
