from a import A

class B:
    def print_As_order(self):
        print "A.order == ", A.order
#        print "A.class_func(4) == ", A.class_func(4)
        
b = B()
b.print_As_order()

def cubeit(x):
    return x**3

def hypercubeit(x):
    return x**4

bar = A(cubeit)
print "bar.order == ", bar.order

print "A.class_func(4) == ", A.class_func(4)

#c = bar.class_func # why doesn't this work??
#print "c(4) == ", c(4) # why doesn't this work??


